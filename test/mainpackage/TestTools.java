package mainpackage;

import java.util.ArrayList;

public class TestTools {
    public static ArrayList<String[]> createTestProcedureArray(){
        ArrayList<String[]> testAnswer = new ArrayList<>();
        String[] line1 = new String[]{"sub", "main"};
        String[] line2 = new String[]{"set", "a", "5"};
        String[] line3 = new String[]{"print", "a"};
        String[] line4 = new String[]{"set", "b", "5"};
        testAnswer.add(line1);
        testAnswer.add(line2);
        testAnswer.add(line3);
        testAnswer.add(line4);
        return testAnswer;
    }

    public static ArrayList<String[]> createMultipleProcedureArray(){
        ArrayList<String[]> testAnswer = new ArrayList<>();
        String[] line1 = new String[]{"sub", "main"};
        String[] line2 = new String[]{"call", "foo"};
        String[] line3 = new String[]{"set", "b", "5"};
        String[] line4 = new String[]{"sub", "foo"};
        String[] line5 = new String[]{"set", "b", "10"};
        testAnswer.add(line1);
        testAnswer.add(line2);
        testAnswer.add(line3);
        testAnswer.add(line4);
        testAnswer.add(line5);
        return testAnswer;
    }

    public static ArrayList<String[]> createTestCompiledArray(){
        ArrayList<String[]> testAnswer = new ArrayList<>();
        String[] line2 = new String[]{"set", "a", "5"};
        String[] line3 = new String[]{"print", "a"};
        String[] line4 = new String[]{"set", "b", "5"};
        testAnswer.add(line2);
        testAnswer.add(line3);
        testAnswer.add(line4);
        return testAnswer;
    }

    public static Environment createTestEnvironment(){
        Compiler compiler = new Compiler(createTestProcedureArray());
        compiler.compile();
        return compiler.getBuild();
    }

    public static Environment createTestCallEnvironment(){
        Compiler compiler = new Compiler(createMultipleProcedureArray());
        compiler.compile();
        return compiler.getBuild();
    }

    public static IntEnvironment createTestIntEnvironment(){
        IntEnvironment test = new IntEnvironment();
        test.setEnvironment(createTestEnvironment());
        test.setProgramText(createTestProcedureArray());
        test.setCurrentLine(test.getEnvironment().getProcedureStorage().getProcedureLocation("main") + 1);
        test.setCurrentProcedure("main");
        return test;
    }

    public static IntEnvironment createTestCallIntEnv(){
        Compiler compiler = new Compiler(createMultipleProcedureArray());
        compiler.compile();
        IntEnvironment test = new IntEnvironment();
        test.setEnvironment(compiler.getBuild());
        test.setProgramText(createMultipleProcedureArray());
        test.setCurrentLine(test.getEnvironment().getProcedureStorage().getProcedureLocation("main") + 1);
        test.getStack().addFirst(new StackTrace("main", "", test.getCurrentLine()));
        test.setCurrentProcedure("main");
        return test;
    }
}
