package mainpackage;

import org.junit.Test;

import static org.junit.Assert.*;

public class CompilerTest {

    Compiler testCompiler = null;

    @Test
    public void compileTest() {
        testCompiler = new Compiler(TestTools.createTestProcedureArray());
        testCompiler.compile();
        assertEquals((int)testCompiler.getBuild().getProcedureStorage().getProcedureLocation("main"), 0);
    }


}