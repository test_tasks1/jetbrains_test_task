package mainpackage;

import org.junit.Test;

import static org.junit.Assert.*;

public class StackTraceTest {

    StackTrace test = new StackTrace("main", "call", 1);

    @Test
    public void getProcedureTest() {
        assertEquals(test.getProcedure(), "main");
    }

    @Test
    public void getLineTest() {
        assertEquals((int)test.getLine(), 1);
    }

    @Test
    public void toStringTest() {
        assertEquals(test.toString(), "2: main call");
    }
}