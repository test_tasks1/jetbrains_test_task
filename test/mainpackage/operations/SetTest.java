package mainpackage.operations;

import mainpackage.Environment;
import mainpackage.TestTools;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SetTest {
    @Test
    public void doOperationTest() {
        Set test = new Set();
        ArrayList<String> args = new ArrayList<>();
        args.add("a");
        args.add("100");
        Environment testEnv = TestTools.createTestEnvironment();
        test.doOperation(args, testEnv);
        assertEquals((int)testEnv.getVariableStorage().get("a"), 100);
    }

}