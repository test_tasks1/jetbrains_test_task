package mainpackage.operations;

import mainpackage.Environment;
import mainpackage.TestTools;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class PrintTest {

    @Test
    public void doOperation() {
        Print test = new Print();
        Environment testEnv = TestTools.createTestEnvironment();
        testEnv.getVariableStorage().put("a", 5);
        ArrayList<String> args = new ArrayList<>();
        args.add("a");
        test.doOperation(args, testEnv);
        assertEquals(testEnv.getOutput().get(0), "op a = 5");
    }
}