package mainpackage.operations;

import mainpackage.Environment;
import mainpackage.TestTools;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class OperationFactoryTest {

    @Test
    public void getOperationTest() {
        ArrayList<String> args = new ArrayList<>();
        args.add("a");
        args.add("100");
        Environment testEnv = TestTools.createTestEnvironment();
        OperationFactory.getInstance().getOperation("set").doOperation(args, testEnv);
        assertEquals((int)testEnv.getVariableStorage().get("a"), 100);
    }

}