package mainpackage.operations;

import mainpackage.Environment;
import mainpackage.TestTools;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class CallTest {

    @Test
    public void doOperation() {
        Call test = new Call();
        ArrayList<String> args = new ArrayList<>();
        args.add("foo");
        Environment testEnv = TestTools.createTestCallEnvironment();
        test.doOperation(args, testEnv);
        assertEquals((int)testEnv.getVariableStorage().get("b"), 10);
    }
}