package mainpackage;

import static org.junit.Assert.*;

public class ProcedureStorageTest {

    @org.junit.Test
    public void procedureTest(){
        ProcedureStorage ps = new ProcedureStorage();
        Procedure test = new Procedure(TestTools.createTestProcedureArray());
        ps.addProcedure("test", 1, test);
        assertEquals(ps.getProcedureByName("test"), test);
        assertEquals((int)ps.getProcedureLocation("test"), 1);
    }
}