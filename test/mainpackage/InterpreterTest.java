package mainpackage;

import org.junit.Test;

import static org.junit.Assert.*;

public class InterpreterTest {


    @Test
    public void nextCommand() {
        Environment testEnv = TestTools.createTestEnvironment();
        Interpreter test = new Interpreter(TestTools.createTestProcedureArray(), testEnv);
        test.nextCommand("step over");
        assertEquals((int)testEnv.getVariableStorage().get("a"), 5);
    }

    @Test
    public void getOutput() {
        Environment testEnv = TestTools.createTestEnvironment();
        Interpreter test = new Interpreter(TestTools.createTestProcedureArray(), testEnv);
        test.nextCommand("step over");
        test.nextCommand("step over");
        assertEquals(test.getOutput().get(0), "op a = 5");
    }

    @Test
    public void getCurrentLine() {
        Environment testEnv = TestTools.createTestEnvironment();
        Interpreter test = new Interpreter(TestTools.createTestProcedureArray(), testEnv);
        test.nextCommand("step over");
        assertEquals((int)test.getCurrentLine(), 2);
    }
}