package mainpackage;

import org.junit.Test;

import static org.junit.Assert.*;

public class IntEnvironmentTest {

    IntEnvironment test = new IntEnvironment();



    @Test
    public void getCurrentProcedure() {
        test.setCurrentProcedure("main");
        assertEquals("main", test.getCurrentProcedure());
    }

    @Test
    public void getEnvironment() {
        Environment testEnv = new Environment();
        test.setEnvironment(testEnv);
        assertEquals(testEnv, test.getEnvironment());
    }

    @Test
    public void getCurrentLine() {
        test.setCurrentLine(0);
        assertEquals((int)test.getCurrentLine(), 0);
    }
}