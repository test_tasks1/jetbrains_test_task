package mainpackage;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ParserTest {

    Parser parser = null;

    @org.junit.Test
    public void parseTest() {
        String testProgram = "sub main\nset a 5\nprint a\n";
        ByteArrayInputStream testStream = new ByteArrayInputStream(testProgram.getBytes());
        parser = new Parser(testStream);
        parser.parse();
        ArrayList<String[]> answer = parser.getLines();
        ArrayList<String[]> testAnswer = TestTools.createTestProcedureArray();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < answer.get(i).length; j++) {
                assertEquals(answer.get(i)[j], testAnswer.get(i)[j]);
            }
        }
    }

}