package mainpackage.commands;

import mainpackage.IntEnvironment;
import mainpackage.TestTools;
import org.junit.Test;

import static org.junit.Assert.*;

public class StepIntoTest {

    @Test
    public void doCommandTest() {
        IntEnvironment test = TestTools.createTestCallIntEnv();
        StepInto testComm = new StepInto();
        testComm.doCommand(test);
        assertEquals(test.getCurrentProcedure(), "foo");
        assertEquals((int)test.getCurrentLine(), 4);
    }
}