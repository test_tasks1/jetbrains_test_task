package mainpackage.commands;

import mainpackage.IntEnvironment;
import mainpackage.TestTools;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommandFactoryTest {

    @Test
    public void getCommand() {
        IntEnvironment env = TestTools.createTestIntEnvironment();
        Command testStep = CommandFactory.getInstance().getCommand("step over");
        testStep.doCommand(env);
        assertEquals((int)env.getCurrentLine(), 2);
        assertEquals((int)env.getEnvironment().getVariableStorage().get("a"), 5);
    }
}