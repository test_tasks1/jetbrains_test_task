package mainpackage.commands;

import mainpackage.IntEnvironment;
import mainpackage.TestTools;
import org.junit.Test;

import static org.junit.Assert.*;

public class TraceTest {

    @Test
    public void doCommand() {
        IntEnvironment test = TestTools.createTestCallIntEnv();
        Trace testComm = new Trace();
        testComm.doCommand(test);
        assertEquals(test.getEnvironment().getOutput().get(0), "comm 2: main ");
    }
}