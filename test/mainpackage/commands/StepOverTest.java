package mainpackage.commands;

import mainpackage.IntEnvironment;
import mainpackage.TestTools;
import org.junit.Test;

import static org.junit.Assert.*;

public class StepOverTest {

    @Test
    public void doCommandTest() {
        IntEnvironment test = TestTools.createTestCallIntEnv();
        StepOver testComm = new StepOver();
        testComm.doCommand(test);
        assertEquals(test.getCurrentProcedure(), "main");
        assertEquals((int)test.getCurrentLine(), 2);
    }
}