package mainpackage.commands;

import mainpackage.IntEnvironment;
import mainpackage.TestTools;
import org.junit.Test;

import static org.junit.Assert.*;

public class VarTest {

    @Test
    public void doCommand() {
        IntEnvironment test = TestTools.createTestIntEnvironment();
        Var testComm = new Var();
        CommandFactory.getInstance().getCommand("step over").doCommand(test);
        testComm.doCommand(test);
        assertEquals("comm a : 5", test.getEnvironment().getOutput().get(0));
    }
}