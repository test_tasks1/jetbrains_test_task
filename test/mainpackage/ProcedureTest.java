package mainpackage;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProcedureTest {

    Procedure test = null;

    @Test
    public void call() {
        test = new Procedure(TestTools.createTestCompiledArray());
        Environment testEnv = new Environment();
        test.call(testEnv);
        assertEquals((int)testEnv.getVariableStorage().get("a"), 5); //see testtools method
        assertEquals(testEnv.getOutput().get(0), "op a = 5");
    }

    @Test
    public void getLength() {
        test = new Procedure(TestTools.createTestCompiledArray());
        assertEquals((int)test.getLength(), 3);
    }
}