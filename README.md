# Jetbrains Guu Interpreter

Сборка: ant
* ant test-compile - скомпилировать юнит-тесты
* ant test - скомпилировать и запустить юнит-тесты
* ant compile - скомпилировать 
* ant jar - скомпилировать и собрать jar
* ant run - скомпилировать, собрать jar и запустить
* ant clean - очистить папку build
