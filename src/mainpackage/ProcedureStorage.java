package mainpackage;

import java.util.HashMap;

public class ProcedureStorage {
    private HashMap<String, Integer> procedureLocation;
    private HashMap<String, Procedure> procedure;

    /**
     * Storage class for @Procedure.
     * You can use it to store procedures
     * and get them by their name or
     * get their locations by their name.
     */
    ProcedureStorage(){
        this.procedure = new HashMap<>();
        this.procedureLocation = new HashMap<>();
    }

    public void addProcedure (String name, Integer lineNumber, Procedure procedure) {
        this.procedureLocation.put(name, lineNumber);
        this.procedure.put(name, procedure);
    }

    public Procedure getProcedureByName (String name) {
        return procedure.get(name);
    }

    public Integer getProcedureLocation (String name) {
        return procedureLocation.get(name);
    }

}
