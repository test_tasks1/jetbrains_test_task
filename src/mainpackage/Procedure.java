package mainpackage;

import mainpackage.operations.Operation;
import mainpackage.operations.OperationFactory;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class contains single
 * Guu procedure as an ArrayList of
 * operations with arguments.
 */

public class Procedure {

    private ArrayList<String[]> operations;
    Procedure(ArrayList<String[]> operations) {
        this.operations = operations;
    }

    /**
     * This method launches a procedure.
     * @param environment - Guu build, in which the procedure is launched.
     */

    public void call(Environment environment){
        try {
            operations.forEach(operation -> {
                if (!String.join("", operation).equals("")) {
                    ArrayList<String> operationArgs = new ArrayList<>(Arrays.asList(operation));
                    String operationName = operationArgs.remove(0);
                    Operation op = OperationFactory.getInstance().getOperation(operationName);
                    op.doOperation(operationArgs, environment);
                }
            });
        } catch (StackOverflowError e) {
            environment.getOutput().add("op stack overflow error");
        }
    }

    public Integer getLength() {
        return operations.size();
    }

}
