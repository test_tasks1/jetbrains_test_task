package mainpackage;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class View {
    private JFrame frame;
    private JTextArea code;
    private JTextArea output;
    private JPanel buttonPanel;
    private Controller controller;

    View(){
        frame = new JFrame();
        frame.setTitle("Guu interpreter");
        frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        ///////code window
        code = new JTextArea(25, 30);
        code.setEditable(true);
        JScrollPane scrollableTextArea = new JScrollPane(code);
        scrollableTextArea.setAutoscrolls(true);
        code.setAutoscrolls(true);
        code.setLineWrap(false);
        DefaultCaret caret = (DefaultCaret) code.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        scrollableTextArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollableTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        frame.add(scrollableTextArea);

        ////////output window
        output = new JTextArea(10, 30);
        output.setEditable(false);
        JScrollPane scrollableOutputArea = new JScrollPane(output);
        scrollableOutputArea.setAutoscrolls(true);
        output.setAutoscrolls(true);
        output.setLineWrap(false);
        DefaultCaret caret2 = (DefaultCaret) output.getCaret();
        caret2.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        scrollableOutputArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollableOutputArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);


        //////buttons
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        controller = new Controller(code, output);
        controller.getButtons().forEach(button -> buttonPanel.add(button));
        frame.add(buttonPanel);

        frame.add(scrollableOutputArea);

        frame.setVisible(true);
        frame.pack();
        frame.setResizable(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}
