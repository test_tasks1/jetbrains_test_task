package mainpackage;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * This parser translates plain text, which is supposed to be Guu code
 * to a Guu code which is easy for compiler to understand.
 */
public class Parser {

    private InputStream inputStream;
    private ArrayList<String[]> lines;

    Parser(InputStream inputStream) {
        this.inputStream = inputStream;
        lines = new ArrayList<>();
    }

    public void parse(){
        Scanner sc = new Scanner(inputStream);
        while (sc.hasNextLine()) {
            String nextLine = sc.nextLine().trim();
            lines.add(nextLine.split(" "));
        }
    }

    public ArrayList<String[]> getLines(){
        return lines;
    }


}
