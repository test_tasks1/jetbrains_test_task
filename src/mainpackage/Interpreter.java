package mainpackage;

import mainpackage.commands.CommandFactory;

import java.util.*;

/**
 * This class interprets a Guu program build
 * step-by-step. Call {@link Interpreter#consoleRun()} method to interpret from console
 * or just send one command at a time by {@link Interpreter#nextCommand(String)}.
 */
public class Interpreter extends Observable {


    private IntEnvironment intEnv;


    /**
     * Constructs new Interpreter based on a program text
     * and this programs build.
     * @param programText  Guu code
     * @param env  Environment, created by compilating Guu code with {@link Compiler#compile()}
     */
    Interpreter(ArrayList<String[]> programText, Environment env) {
        intEnv = new IntEnvironment();
        intEnv.setProgramText(programText);
        intEnv.setEnvironment(env);
        try {
            intEnv.setCurrentLine(intEnv
                    .getEnvironment()
                    .getProcedureStorage()
                    .getProcedureLocation("main") + 1);
            skipEmptyLines();
            //get main procedure location
            intEnv.getStack().addFirst(new StackTrace("main", "",
                    intEnv
                            .getEnvironment()
                            .getProcedureStorage()
                            .getProcedureLocation("main")));
            intEnv.setCurrentProcedure("main");
            //print the first line
            //System.out.println(intEnv.getCurrentLine() + " " + Arrays.toString(intEnv.getProgramText().get(intEnv.getCurrentLine())));

        } catch (IllegalArgumentException e) {
            System.out.println("No main procedure");
        }
    }



    /**
     * This method makes interpreter to do next command,
     * which is generated from argument.
     * @param command - desired command.
     */

    public void nextCommand(String command) {
        CommandFactory.getInstance().getCommand(command).doCommand(intEnv);
        //if program text got empty strings we need to skip them
        skipEmptyLines();
        //if the procedure has ended, return to where from it was called
        //or finish interpreting if returning from main
        while (isReturnFromProcedure()){
            if (intEnv.getStack().size() == 1) {
                setChanged();
                notifyObservers();
                throw new IndexOutOfBoundsException("Program has ended");
            }
            Integer whereToReturn = intEnv.getStack().getFirst().getLine();
            intEnv.setCurrentLine(whereToReturn + 1);
            intEnv.setCurrentProcedure(intEnv.getStack().getFirst().getProcedure());
            intEnv.getStack().removeFirst();
            skipEmptyLines();

        }
        setChanged();
        notifyObservers();
        //print current line
        //System.out.println(intEnv.getCurrentLine() + " " + Arrays.toString(intEnv.getProgramText().get(intEnv.getCurrentLine())));
    }
    /**
     * This method initializes interpreter
     * by looking for main procedure
     * and setting its first line as current line,
     * and starts getting commands from console.
     * WARNING: DEBUG METHOD
     */
    private void consoleRun(){
        //run();
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()) {
            //awaits for command
            String command = sc.nextLine();
            nextCommand(command);
        }
    }

    private void skipEmptyLines(){
        if (intEnv.getCurrentLine() != intEnv.getProgramText().size())
            while(String.join("", intEnv.getProgramText().get(intEnv.getCurrentLine())).equals("")){
                intEnv.setCurrentLine(intEnv.getCurrentLine() + 1);
            }
    }

    /**
     * This method checks if
     * the procedure you are currently in
     * has ended.
     * @return true if procedure is ended, false if not
     */

    private boolean isReturnFromProcedure(){
        Integer currentProcedureStart = intEnv
                .getEnvironment()
                .getProcedureStorage()
                .getProcedureLocation(intEnv.getCurrentProcedure());
        Integer currentProcedureLength = intEnv
                .getEnvironment()
                .getProcedureStorage()
                .getProcedureByName(intEnv.getCurrentProcedure())
                .getLength();
        return currentProcedureStart + currentProcedureLength < intEnv.getCurrentLine();
    }

    public ArrayList<String> getOutput(){
        return this.intEnv.getEnvironment().getOutput();
    }

    public Integer getCurrentLine(){
        return intEnv.getCurrentLine();
    }


}
