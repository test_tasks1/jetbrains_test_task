package mainpackage;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * Environment is a storage for everything
 * you need to run a Guu program: Guu procedures and variables.
 * Also, it has outStream for output redirection.
 * It is {@link System#out} by default.
 */
public class Environment {
    private ProcedureStorage procedureStorage;
    private HashMap<String, Integer> variableStorage;
    private ArrayList<String> output;



    Environment(){
        procedureStorage = new ProcedureStorage();
        variableStorage = new HashMap<>();
        output = new ArrayList<>();
    }

    public ArrayList<String> getOutput() {
        return output;
    }

    /*public void setOutStream(PrintStream outStream) {
        this.outStream = outStream;
    }
    */
    public ProcedureStorage getProcedureStorage() {
        return procedureStorage;
    }

    public HashMap<String, Integer> getVariableStorage() {
        return variableStorage;
    }
}
