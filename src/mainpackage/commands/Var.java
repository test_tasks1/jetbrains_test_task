package mainpackage.commands;

import mainpackage.IntEnvironment;

public class Var implements Command {
    @Override
    public void doCommand(IntEnvironment env) {
        env.getEnvironment().getVariableStorage().forEach((key, value)
                -> env.getEnvironment().getOutput().add("comm " + key + " : " + value));
    }
}
