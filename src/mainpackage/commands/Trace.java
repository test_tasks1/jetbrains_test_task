package mainpackage.commands;

import mainpackage.IntEnvironment;

import java.util.Collections;
import java.util.Iterator;

public class Trace implements Command {
    @Override
    public void doCommand(IntEnvironment env) {
        Iterator it = env.getStack().descendingIterator();
        while (it.hasNext()) {
            env.getEnvironment().getOutput().add("comm " + it.next().toString());
        }
    }
}
