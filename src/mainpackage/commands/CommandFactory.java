package mainpackage.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class CommandFactory {
    private HashMap<String, Command> commands;
    private static CommandFactory instance;


    private CommandFactory() {
        commands = new HashMap<>();
    }


    public Command getCommand(String commandName) {
        if (commands.get(commandName) != null)
            return commands.get(commandName);
        else {
            ArrayList<String> command = new ArrayList<>(Arrays.asList(commandName.split(" ")));
            StringBuilder sb = new StringBuilder();
            for (String s : command)
                sb.append(s.substring(0, 1).toUpperCase()).append(s.substring(1));
            String desComm = sb.toString();
            try {
                commands.put(commandName, (Command) Class
                        .forName("mainpackage.commands." + desComm)
                        .getDeclaredConstructor()
                        .newInstance());
            } catch (Exception e) {
                throw new IllegalArgumentException("No such command: " + commandName);
            }
            return commands.get(commandName);
        }
    }


    public static CommandFactory getInstance() {
        if (instance == null) {
            instance = new CommandFactory();
        }
        return instance;
    }
}
