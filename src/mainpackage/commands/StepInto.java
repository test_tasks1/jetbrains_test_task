package mainpackage.commands;

import mainpackage.IntEnvironment;
import mainpackage.StackTrace;
import mainpackage.operations.OperationFactory;

import java.util.ArrayList;
import java.util.Arrays;

public class StepInto implements Command {
    @Override
    public void doCommand(IntEnvironment env) {
        while(String.join("", env.getProgramText().get(env.getCurrentLine())).equals("")){
            env.setCurrentLine(env.getCurrentLine() + 1);
        }
        ArrayList<String> currentCommand = new ArrayList<>(Arrays.asList(env.getProgramText().get(env.getCurrentLine())));
        String opName = currentCommand.remove(0);
        if ("call".equals(opName)) {
            env.getStack().addFirst(new StackTrace(env.getCurrentProcedure(), "call", env.getCurrentLine()));
            try {
                env.setCurrentLine(env.getEnvironment().getProcedureStorage().getProcedureLocation(currentCommand.get(0)) + 1);
            } catch (NullPointerException e) {
                throw new IllegalArgumentException("No such procedure: " + currentCommand.get(0));
            }
            env.setCurrentProcedure(currentCommand.get(0));
        } else {
            OperationFactory.getInstance().getOperation(opName).doOperation(currentCommand, env.getEnvironment());
            env.setCurrentLine(env.getCurrentLine() + 1);
        }
    }
}
