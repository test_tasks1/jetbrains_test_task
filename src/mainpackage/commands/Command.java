package mainpackage.commands;

import mainpackage.IntEnvironment;

/**
 * Interface for every interpreter command
 * of this Guu interpreter. To add new command, you must
 * implement this interface. If command must print something,
 * add "comm " at the start of each line, so output
 * could distinguish it from operator prints.
 */
public interface Command {
    public void doCommand (IntEnvironment env);
}
