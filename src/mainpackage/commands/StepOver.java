package mainpackage.commands;

import mainpackage.IntEnvironment;
import mainpackage.operations.OperationFactory;

import java.util.ArrayList;
import java.util.Arrays;

public class StepOver implements Command {
    @Override
    public void doCommand(IntEnvironment env) {
        while(String.join("", env.getProgramText().get(env.getCurrentLine())).equals("")){
            env.setCurrentLine(env.getCurrentLine() + 1);
        }
        ArrayList<String> currentCommand = new ArrayList<>(Arrays.asList(env.getProgramText().get(env.getCurrentLine())));
        String opName = currentCommand.remove(0);
        OperationFactory.getInstance().getOperation(opName).doOperation(currentCommand, env.getEnvironment());
        env.setCurrentLine(env.getCurrentLine() + 1);
    }
}
