package mainpackage.operations;

import mainpackage.Environment;

import java.util.ArrayList;

/**
 * Interface for every operation in Guu.
 * To add new operator, you must implement this interface.
 * If operator prints something, you must add "op " at the start of each print line
 * so output can distinguish it from interpreter prints.
 */
public interface Operation {
    public void doOperation (ArrayList<String> args, Environment environment);
}
