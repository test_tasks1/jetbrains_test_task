package mainpackage.operations;

import mainpackage.Environment;

import java.util.ArrayList;

public class Set implements Operation {
    public void doOperation(ArrayList<String> args, Environment environment) {
        if (args.size() != 2)
            throw new IllegalArgumentException("Wrong number of arguments");
        try {
            environment.getVariableStorage().put(args.get(0), Integer.parseInt(args.get(1)));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Wrong variable format");
        }
    }
}
