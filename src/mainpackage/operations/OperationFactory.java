package mainpackage.operations;

import java.util.HashMap;

public class OperationFactory {
    private HashMap<String, Operation> operations;
    private static OperationFactory instance;


    private OperationFactory() {
        operations = new HashMap<>();
    }


    /**
     * Tries to get {@link mainpackage.operations.Operation} by its name.
     *
     * @param operationName desired operation name
     * @return desired operation
     * @throws IllegalArgumentException if no such operation was found
     */

    public Operation getOperation(String operationName) {
        if (operations.get(operationName) != null)
            return operations.get(operationName);
        else  {
            String desOp = operationName.substring(0, 1).toUpperCase() + operationName.substring(1);
            try {
                operations.put(operationName, (Operation) Class
                        .forName("mainpackage.operations." + desOp)
                        .getDeclaredConstructor()
                        .newInstance());
            } catch (Exception e) {
                throw new IllegalArgumentException("No such operation: " + operationName);
            }
            return operations.get(operationName);
        }
    }


    public static OperationFactory getInstance() {
        if (instance == null) {
            instance = new OperationFactory();
        }
        return instance;
    }
}
