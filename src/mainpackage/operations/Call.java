package mainpackage.operations;

import mainpackage.Environment;

import java.util.ArrayList;

public class Call implements Operation {
    @Override
    public void doOperation(ArrayList<String> args, Environment environment) {
        if (args.size() != 1) {
            throw new IllegalArgumentException("Wrong number of arguments");
        }
        String pName = args.get(0);
        if (environment.getProcedureStorage().getProcedureByName(pName) == null)
            throw new IllegalArgumentException("No such procedure: " + pName);
        else
            environment.getProcedureStorage().getProcedureByName(pName).call(environment);
    }
}
