package mainpackage.operations;

import mainpackage.Environment;

import java.util.ArrayList;

public class Print implements Operation {

    @Override
    public void doOperation(ArrayList<String> args, Environment environment) {
        if (args.size() != 1) {
            throw new IllegalArgumentException("Wrong number of arguments");
        }
        Integer var = environment.getVariableStorage().get(args.get(0));
        if (var == null)
            throw new IllegalArgumentException("No such variable: " + args.get(0));
        else
            environment.getOutput().add("op " + args.get(0) + " = " + var);
    }
}
