package mainpackage;
/**
 * Class for a single line of a stack trace.
 * Format of stack trace: line, where operation was called +
 * name of procedure, where operation was called + name of operation.
 */
public class StackTrace {
    private String procedure;
    private String operation;
    private Integer line;



    public StackTrace(String procedure, String operation, Integer line) {
        this.procedure = procedure;
        this.operation = operation;
        this.line = line;
    }


    public String getProcedure() {
        return procedure;
    }


    public Integer getLine() {
        return line;
    }

    public String toString(){
        int lineNumber = line + 1;
        return lineNumber + ": " + procedure + " " + operation;
    }
}
