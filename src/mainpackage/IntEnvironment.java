package mainpackage;


import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 * Interpretation Environment is a storage class for everything
 * you need to interpret a Guu program: {@link Environment},
 * stack of run procedures, current position in program.
 */

public class IntEnvironment {
    private ArrayList<String[]> programText;
    private Environment environment;
    private ArrayDeque<StackTrace> stack;
    private String currentProcedure;
    private Integer currentLine;


    IntEnvironment(){
        this.stack = new ArrayDeque<>();
    }

    public ArrayList<String[]> getProgramText() {
        return programText;
    }

    public String getCurrentProcedure() {
        return currentProcedure;
    }

    public void setCurrentProcedure(String currentProcedure) {
        this.currentProcedure = currentProcedure;
    }

    public void setProgramText(ArrayList<String[]> programText) {
        this.programText = programText;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public ArrayDeque<StackTrace> getStack() {
        return stack;
    }

    public Integer getCurrentLine() {
        return currentLine;
    }

    public void setCurrentLine(Integer currentLine) {
        this.currentLine = currentLine;
    }
}
