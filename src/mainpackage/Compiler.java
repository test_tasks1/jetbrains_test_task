package mainpackage;

import java.util.ArrayList;

/**
 *  This class translates code
 *  to {@link ProcedureStorage} in environment
 *  and runs it, starting from main procedure.
 *  Use {@link Compiler#compile()} method to create procedures,
 *  {@link Compiler#getBuild()} method to extract enviroment
 *  (for example, to put it into interpreter).
 */

public class Compiler {

    private ArrayList<String[]> programText;
    private Environment environment;

    Compiler(ArrayList<String[]> programText) {
        this.programText = programText;
        this.environment = new Environment();
    }

    /**
     * Tries to generate {@link ProcedureStorage} from code.
     *
     * @throws IllegalArgumentException if operation is out of procedure.
     * For example, program that looks like:
     *  <blockquote><pre>
     *     set a 5
     *  </pre></blockquote><p>
     *   will generate this exception because of line number 1.
     *   Also, exception will be thrown if there is no main procedure.
     */

    public void compile(){
        Integer lineNumber;
        for (lineNumber = 0; lineNumber < programText.size(); lineNumber++) {
            String[] currentLine = programText.get(lineNumber);
            if (currentLine[0].equals("sub")){
                String procedureName = currentLine[1];
                Integer procStart = lineNumber;

                ArrayList<String[]> operations = new ArrayList<>();
                while (lineNumber != programText.size() - 1 && !programText.get(++lineNumber)[0].equals("sub")){
                    operations.add(programText.get(lineNumber));
                }
                if (lineNumber != programText.size() - 1) lineNumber--;
                environment.getProcedureStorage().addProcedure(procedureName, procStart, new Procedure(operations));
            } else {
                throw new IllegalArgumentException("Operation out of procedure");
            }
        }
        if (environment.getProcedureStorage().getProcedureByName("main") == null) throw new IllegalArgumentException("No main procedure");
    }

    /**
     * @return {@link Environment} - compiler environment, which
     * is also a build (if {@link Compiler#compile()} has been called before).
     */

    public Environment getBuild(){
        return environment;
    }


}
