package mainpackage;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.DefaultHighlighter;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Controller implements Observer {
    private ArrayList<JButton> buttons;
    private JTextArea code;
    private JTextArea output;
    private Interpreter interpreter;
    private Compiler compiler;
    private ArrayList<String> outputLines;


    public Controller(JTextArea code, JTextArea output) {
        this.code = code;
        this.output = output;
        buttons = new ArrayList<>();
        outputLines = new ArrayList<>();
        ///////buttons
        JButton out = new JButton("Output");
        out.setEnabled(false);
        buttons.add(out);
        out.addActionListener(e -> {
            output.setText("");
            StringBuilder sb = new StringBuilder();
            outputLines.forEach(line -> sb.append(line).append("\n"));
            output.setText(sb.toString().trim());
        });


        JButton c = new JButton("Build and debug");
        buttons.add(c);
        c.addActionListener(e -> {
            try {
                Parser parser = new Parser(new ByteArrayInputStream(code.getText().getBytes()));
                parser.parse();
                compiler = new Compiler(parser.getLines());
                compiler.compile();
                output.setText("Compilation successful");
                outputLines.clear();
                interpreter = new Interpreter(parser.getLines(), compiler.getBuild());
                interpreter.addObserver(this);
                code.getHighlighter().removeAllHighlights();
                code.getHighlighter().addHighlight(code.getLineStartOffset(interpreter.getCurrentLine()),
                        code.getLineEndOffset(interpreter.getCurrentLine()),
                        new DefaultHighlighter.DefaultHighlightPainter(Color.RED));
                buttons.forEach(button -> button.setEnabled(true));
            } catch (IllegalArgumentException e1) {
                output.setText(e1.getMessage());
            } catch (BadLocationException e1) {
                code.getHighlighter().removeAllHighlights();
            }
        });
        JButton i = new JButton("Into");
        buttons.add(i);
        i.setEnabled(false);
        i.addActionListener(e -> {
            try {
                interpreter.nextCommand("step into");
            } catch (IllegalArgumentException e1) {
                output.setText(e1.getMessage());
                code.getHighlighter().removeAllHighlights();
                buttons.forEach(button -> {
                            if (!(button.equals(c) || button.equals(out)))
                                button.setEnabled(false);
                        }
                );
            } catch (IndexOutOfBoundsException e2) {
                code.getHighlighter().removeAllHighlights();
                buttons.forEach(button -> {
                            if (!(button.equals(c) || button.equals(out)))
                                button.setEnabled(false);
                        }
                );
            }
        });

        JButton o = new JButton("Over");
        buttons.add(o);
        o.setEnabled(false);
        o.addActionListener(e -> {
            try {
                interpreter.nextCommand("step over");
            } catch (IllegalArgumentException e1) {
                output.setText(e1.getMessage());
                code.getHighlighter().removeAllHighlights();
                buttons.forEach(button -> {
                            if (!(button.equals(c) || button.equals(out)))
                                button.setEnabled(false);
                        }
                );
            } catch (IndexOutOfBoundsException e2) {
                code.getHighlighter().removeAllHighlights();
                buttons.forEach(button -> {
                            if (!(button.equals(c) || button.equals(out)))
                                button.setEnabled(false);
                        }
                );
            }
        });

        JButton trace = new JButton("Trace");
        buttons.add(trace);
        trace.setEnabled(false);
        trace.addActionListener(e -> {
            try {
                interpreter.nextCommand("trace");
            } catch (IndexOutOfBoundsException e2) {
                code.getHighlighter().removeAllHighlights();
                buttons.forEach(button -> {
                            if (!(button.equals(c) || button.equals(out)))
                                button.setEnabled(false);
                        }
                );
            }
        });

        JButton var = new JButton("Var");
        buttons.add(var);
        var.setEnabled(false);
        var.addActionListener(e -> {
            try {
                interpreter.nextCommand("var");
            } catch (IndexOutOfBoundsException e2) {
                code.getHighlighter().removeAllHighlights();
                buttons.forEach(button -> {
                            if (!(button.equals(c) || button.equals(out)))
                                button.setEnabled(false);
                        }
                );
            }
        });
    }

    @Override
    public void update(Observable o, Object arg) {
        //highlight current line
        try {
            code.getHighlighter().removeAllHighlights();
            code.getHighlighter().addHighlight(code.getLineStartOffset(((Interpreter) o).getCurrentLine()),
                    code.getLineEndOffset(((Interpreter) o).getCurrentLine()),
                    new DefaultHighlighter.DefaultHighlightPainter(Color.RED));
        } catch (BadLocationException e) {
            code.getHighlighter().removeAllHighlights();
        }
        //if observable got something we need to print
        if (((Interpreter) o).getOutput().size() != 0) {
            //we keep output in memory because we don't want it to disappear
            //to distinguish it from commands such as trace or var we add flag
            final Boolean[] hasOutputChanged = {false};
            output.setText("");
            //for each line of output we get its flag
            //and either print it immediately or just save
            ((Interpreter) o).getOutput().forEach(line -> {
                ArrayList<String> outputString = new ArrayList<>(Arrays.asList(line.split(" ")));
                String type = outputString.remove(0);
                StringBuilder sb = new StringBuilder();
                outputString.forEach(e -> sb.append(e).append(" "));
                if (type.equals("op")) {
                    outputLines.add(sb.toString().trim());
                    hasOutputChanged[0] = true;
                } else {
                    output.setText(output.getText() + sb.toString().trim() + "\n");
                }
            });
            //if output changed (it can't happen at the same time with
            //trace or var command), print it immediately
            ((Interpreter) o).getOutput().clear();
            if (hasOutputChanged[0]) {
                output.setText("");
                StringBuilder sb = new StringBuilder();
                outputLines.forEach(line -> sb.append(line).append("\n"));
                output.setText(sb.toString().trim());
            }
        }

    }

    public ArrayList<JButton> getButtons() {
        return buttons;
    }
}
